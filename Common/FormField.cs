﻿namespace Common
{
    public class FormField
    {
        public string Path { get; set; }
        public string Value { get; set; }
    }
}