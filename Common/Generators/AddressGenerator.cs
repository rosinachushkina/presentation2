﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Generators
{
    public class AddressGenerator
    {
        private static readonly List<string> _dumpAddress = new List<string>
        {
            "Kiev",
            "New York",
            "Chernigov",
            "Kostroma",
            "London",
            "Berlin",
            "Drezden",
            "Moskow"
        };

        public static string GetAddress()
        {
            var rnd = new Random();
            return _dumpAddress[rnd.Next(_dumpAddress.Count - 1)];
        }


    }
}
