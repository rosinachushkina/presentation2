﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Generators
{
    public static class PasswordGenerator
    {
        private static readonly Dictionary<string, string> storedPassword = new Dictionary<string, string>();

        public static string GeneratePassword()
        {
            return RandomString(20);
        }

        private static string RandomString(int size)
        {
            var rnd = new Random((int)DateTime.Now.Ticks);
            var builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * rnd.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

    }
}