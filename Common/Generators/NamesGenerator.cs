﻿using System;
using System.Collections.Generic;

namespace Common.Generators
{
    public static class NamesGenerator
    {
        private static readonly List<string> _dumpNames = new List<string>
        {
            "Rosina",
            "Viatly",
            "Matt",
            "Max",
            "Kathryn"
        };

        private static readonly List<string> _dumpLastNames = new List<string> {"Reit", "Rotyur", "Rada", "Chuskina", "Bukova"};

        public static string GetFirstName()
        {
            var rnd = new Random();
            return _dumpNames[rnd.Next(_dumpNames.Count - 1)];
        }

        public static string GetLastName()
        {
            var rnd = new Random();
            return _dumpLastNames[rnd.Next(_dumpNames.Count - 1)];
        }
    }
}