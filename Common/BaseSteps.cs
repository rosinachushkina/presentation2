﻿#region Usings

using System.Collections.Generic;
using System.IO;
using System.Reflection;
using OpenQA.Selenium;
using System.Text;
using System;
using OpenQA.Selenium.IE;
using TechTalk.SpecFlow;
using NUnit.Framework;

#endregion

namespace Common
{
    public class BaseSteps
    {
        public static IWebDriver WebDriver;
        public static IWebDriver WebDriver2;
        public static IWebDriver WebDriver3;
        public static IWebDriver CurrentDriver;
        public static int StandardTimeOut = 10;

    }


    [Binding]
    public class ScenarioSupport
    {
        [BeforeScenario]
        [SetUp]
        public static void BeforeTestRun()
        {
            // BaseSteps.WebDriver = new FirefoxDriver();
            BaseSteps.WebDriver = new InternetExplorerDriver();
            BaseSteps.WebDriver.Manage().Cookies.DeleteAllCookies();
            BaseSteps.WebDriver.Manage().Window.Maximize();
            BaseSteps.WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
            BaseSteps.CurrentDriver = BaseSteps.WebDriver;
        }

    
        
        [AfterScenario]
        public static void AfterTestRun()
        {
            //creation screenshots after test's failing 
            if (TestContext.CurrentContext.Result.Status == TestStatus.Failed)
            {
                Screenshot ss = ((ITakesScreenshot)BaseSteps.WebDriver).GetScreenshot();
                ss.SaveAsFile(string.Format(@"c:\failedTests\{0}{1}.jpg",
                    TestContext.CurrentContext.Test.Name.ToString(), DateTime.Now.ToString("MM-dd-yy.H-mm")),
                    System.Drawing.Imaging.ImageFormat.Jpeg);

                ss = ((ITakesScreenshot)BaseSteps.WebDriver2).GetScreenshot();
                ss.SaveAsFile(string.Format(@"c:\failedTests\{0}{1}WD2.jpg",
                    TestContext.CurrentContext.Test.Name.ToString(), DateTime.Now.ToString("MM-dd-yy.H-mm")),
                    System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            if (BaseSteps.WebDriver != null)
            { BaseSteps.WebDriver.Quit(); BaseSteps.WebDriver = null; }

            if (BaseSteps.WebDriver2 != null)
            { BaseSteps.WebDriver2.Quit(); BaseSteps.WebDriver2 = null; }

            if (BaseSteps.WebDriver3 != null)
            { BaseSteps.WebDriver3.Quit(); BaseSteps.WebDriver3 = null; }
        }
    }






    public class CreationWebDrivers : BaseSteps
    {
        public static void runningWebDriver(string createdWebDriver)
        {
            switch (createdWebDriver)
            {
                case "WebDriver2":          //но здесь лучше приписать его как currentDriver, вместо копипасты

                    WebDriver2 = new InternetExplorerDriver();
                    BaseSteps.WebDriver2.Manage().Cookies.DeleteAllCookies();
                    BaseSteps.WebDriver2.Manage().Window.Maximize();
                    BaseSteps.WebDriver2.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
                    break;

                case "WebDriver3":

                    WebDriver3 = new InternetExplorerDriver();
                    BaseSteps.WebDriver3.Manage().Cookies.DeleteAllCookies();
                    BaseSteps.WebDriver3.Manage().Window.Maximize();
                    BaseSteps.WebDriver3.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
                    break;
            }
        }
    }





    public class FileBasedSteps : BaseSteps
    {

        public static List<string> Images = new List<string>()
        {
            Path.Combine(Path.GetTempPath(),"image.PNG"),
            Path.Combine(Path.GetTempPath(),"thumbnail.PNG")
        };

        public static void InitFiles()
        {
            foreach (var image in Images)
            {
                Assembly a = Assembly.GetCallingAssembly();
                Stream s = a.GetManifestResourceStream("PartsBee.Files." + Path.GetFileName(image));
                BinaryReader sr = new BinaryReader(s);
                var sw = File.Create(image);
                byte[] bytes = sr.ReadBytes((int)s.Length);

                sw.Write(bytes, 0, bytes.Length);
                sw.Flush();
                sw.Close();
                sr.Close();
            }
        }
        public static void DisposeFiles()
        {
            foreach (var image in Images)
            {
                if (File.Exists(image))
                    File.Delete(image);
            }
        }

    }
}