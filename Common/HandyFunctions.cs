﻿using System;                              //Здесь хранятся приятные няшки для работы с автотестами.
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NUnit.Framework;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace Common
{
    public class HandyFunctions : BaseSteps
    {
        public Random random = new Random(Environment.TickCount);
 
        //------------------------Метод создания рандомной латинской строки с цифрами
        public string RandomStringLatinAndNumbers(int length)
        {
            //Набор из которого будет состоять рандомная строка
            string chars = "0123456789abcdefghijklmnopqrstuvwxyz"; 
            StringBuilder builder = new StringBuilder(length);

            for (int i = 0; i < length; ++i)
                builder.Append(chars[random.Next(chars.Length)]);

            return builder.ToString();
        }

        //------------------------Метод создания рандомной латинской строки
        public string RandomStringLatin(int length)
        {
            //Набор из которого будет состоять рандомная строка
            string chars = "abcdefghijklmnopqrstuvwxyz";
            StringBuilder builder = new StringBuilder(length);

            for (int i = 0; i < length; ++i)
                builder.Append(chars[random.Next(chars.Length)]);

            return builder.ToString();
        }

        public string RandomStringCyrillic(int length)
        {
            //Набор из которого будет состоять рандомная строка
            string chars = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            StringBuilder builder = new StringBuilder(length);

            for (int i = 0; i < length; ++i)
                builder.Append(chars[random.Next(chars.Length)]);

            return builder.ToString();
        }

        //------------------------Метод создания рандомной числовой строки
        public string RandomStringNumbers(int length)
        {
            //Набор из которого будет состоять рандомная строка
            string chars = "0123456789"; 
            StringBuilder builder = new StringBuilder(length);

            for (int i = 0; i < length; ++i)
                builder.Append(chars[random.Next(chars.Length)]);

            return builder.ToString();
        }

        //------------------------Метод создания рандомной цисловой строки без 0
        public string RandomStringNumberWithOutZero(int length)
        {
            //Набор из которого будет состоять рандомная строка
            string chars = "123456789";
            StringBuilder builder = new StringBuilder(length);

            for (int i = 0; i < length; ++i)
                builder.Append(chars[random.Next(chars.Length)]);

            return builder.ToString();
        }

        //--------------Ждет нужный элемент по переданному хпасу и максимальным позволительным мсек// данная няшка неактуальна по моему субъективному мнению (с)
                                                                                                                                                       //Roman P.
 /*       public static void WaitForElement_XPATH(string element, int miliseconds)
        {
            int TimePassed = 0;
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(0));

            WaitingForTheElement:
            if(TimePassed<miliseconds)
            try
            {
                WebDriver.FindElement(By.XPath(element));
                try
                    {
                    Assert.IsTrue(WebDriver.FindElement(By.XPath(element)).Displayed);
                    }
                catch (AssertionException)
                    {
                    TimePassed += 100;
                    Thread.Sleep(100);
                    goto WaitingForTheElement;
                    }

            }
            catch (WebDriverException)
            {
                TimePassed += 100;
                Thread.Sleep(100);
                goto WaitingForTheElement;
            }
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(StandardTimeOut));
        } */

        //--------------------Данная няшка убирает из строки двойные пробелы
        public static string DoubleSpaceDeletion(string input)
        {
        string s;
            int n = input.Length;
            s = input;

            if(s.Length>1)
            for (int i = 0; i<n-1; i++)
            {
                if (s[i] == ' ' && s[i + 1] == ' ')
                {
                    s = s.Remove(i, 1);
                    n--;
                    i--;
                }
            }
            
            return s;
        }

        //--------------------Данная не менее няшная няшка убирает из строки все, кроме цифр и сепараторов
        public static string TrimToDigitsAndSeparators(string input)
            {
            string s;
            int n = input.Length;
            s = input;
            string TargetChars = "0123456789,."; 

            if (s.Length > 1)
                for (int i = 0; i < n; i++)
                    {
                    if (!TargetChars.Contains(s[i]))
                        {
                        s = s.Remove(i, 1);
                        n--;
                        i--;
                        }
                    }

            return s;
            }

        //--------------------Данная не менее няшная няшка убирает из строки все, кроме цифр
        public static string TrimToDigits(string input)
            {
            string s;
            int n = input.Length;
            s = input;
            string TargetChars = "0123456789";

            if (s.Length > 1)
                for (int i = 0; i < n; i++)
                    {
                    if (!TargetChars.Contains(s[i]))
                        {
                        s = s.Remove(i, 1);
                        n--;
                        i--;
                        }
                    }

            return s;
            }

        //--------------------Конвертация строки с Days, Hours, Minutes, Seconds в секунды
      
        public static int ConvertStringDHMSToSeconds(string input)
            {
            string s;
            int n = input.Length;
            s = input;
            int days=0;
            int hours=0; 
            int minutes=0;
            int seconds = 0;
            int r;

            if (s.Contains("Day"))
                {
                days = Convert.ToInt32(s.Substring(0, s.IndexOf("Day")));
                int k = s.IndexOf("Day");
                s = s.Remove(0, k + 4);
                s.Trim();
                }
            if (s.Contains("Hour"))
                {
                hours = Convert.ToInt32(s.Substring(0, s.IndexOf("Hour")));
                int k = s.IndexOf("Hour");
                s = s.Remove(0, k + 5);
                s.Trim();
                }
            if (s.Contains("Minute"))
                {
                minutes = Convert.ToInt32(s.Substring(0, s.IndexOf("Minute")));
                int k = s.IndexOf("Minute");
                s = s.Remove(0, k + 7);
                s.Trim();
                }
            if (s.Contains("Second"))
                {
                seconds = Convert.ToInt32(s.Substring(0, s.IndexOf("Second")));
                int k = s.IndexOf("Second");
                s = s.Remove(0, k + 7);
                s.Trim();
                }

            r = seconds + minutes * 60 + hours * 3600 + days * 86400;

            return r;
            }

        //--------------------Данная не менее няшная няшка забирает штат в формате CC из строки
        public static string TrimToStateAcro(string input)
            {
            string s,r;
            int n = input.Length;
            s = input;
            r = "";

            if (s.Length >= 4)
                for (int i = 0; i < n-3; i++)
                    {
                    if (s[i]==' ' && s[i+1]!=' ' && s[i+2]!=' ' && s[i+3]==' ')
                        {
                        r = s[i].ToString() + s[i + 1].ToString() + s[i + 2].ToString() + s[i + 3].ToString();
                        }
                    }
            r = r.Trim();
            r = r.ToUpper();
            return r;
            }

        //--------------------Данная няшка убирает из строки нужную подстроку
        public static string SubstringRemove(string input, string rem)
            {
            string s;
            int n = input.Length;
            s = input;

            while (s.Contains(rem))
                s = s.Remove(s.IndexOf(rem), rem.Length);
            return s;
            }

        //---------------------Данная няшка выполняет скрипт входа в систему после смены логина
        public static string MakeLoginAs(string input)
        {
            string s = input;         
            string file_path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())) + @"\Scripts\" +s+".exe";
            
            Process.Start(file_path);
            Thread.Sleep(5000);
            return s;

            

            
        }

    }
}
