﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Common.Queries
{
    internal static class QueryProcessor
    {
        private const string WHITE_SPACE = " ";
        private const string ID_PATH = "#";
        private const string X_PATH = "/";

        private static IWebElement InternalProcessQuery(ElementsQuery query)
        {
            if (query.Selector.StartsWith(ID_PATH) && query.Selector.Contains(WHITE_SPACE))
                return query.WebDriver.GetById(query.Selector.Remove(0, 1));

            if (query.Selector.StartsWith(X_PATH))
                return query.WebDriver.GetByPath(query.Selector);

            return query.WebDriver.GetByCss(query.Selector);
        }

        public static IWebElement Process(ElementsQuery query)
        {
            if (query.TimeOut != 0)
            {
                var wait = new WebDriverWait(query.WebDriver, TimeSpan.FromMilliseconds(query.TimeOut));
                wait.Until(webDriver => InternalProcessQuery(query));
            }
            return InternalProcessQuery(query);
        }
    }
}