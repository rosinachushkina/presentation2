﻿using OpenQA.Selenium;

namespace Common.Queries
{
    public class ElementsQuery
    {
        private readonly IWebDriver _webDriver;

        public ElementsQuery(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public string Selector { get; set; }
        public int TimeOut { get; set; }

        public IWebDriver WebDriver
        {
            get { return _webDriver; }
        }

        public ElementsQuery SetTimer(int milliSeconds)
        {
            TimeOut = milliSeconds;
            return this;
        }

        public IWebElement Find()
        {
            return QueryProcessor.Process(this);
        }
    }
}