﻿using System.Linq;
using System.Net;
using System.Net.Http;

namespace Common.Helpers
{
    public static class RequestHelper
    {
        private static readonly HttpStatusCode[] _successCodes = {HttpStatusCode.OK, HttpStatusCode.NotModified};

        public static bool Exists(string url)
        {
            var message = new HttpRequestMessage(HttpMethod.Head, url);
            var client = new HttpClient();
            return _successCodes.Contains(client.SendAsync(message).Result.StatusCode);
        }
    }
}