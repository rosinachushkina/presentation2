﻿using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TechTalk.SpecFlow;


namespace Stable_2013.Scenarios
{
    [Binding]
    public class RunningDriversSteps : BaseSteps
    {
        [Then(@"I want to run (.*)")]
        public void ThenIWantToRunWebDriver(string k)
        {
            CreationWebDrivers.runningWebDriver(k);
        }

        [Then(@"I activate (.*)")]
        public void ThenISwitchToWebDriver(string k)
        {
            if (k.Equals("WebDriver"))
            {
                CurrentDriver = WebDriver;
            }
            if (k.Equals("WebDriver2"))
            {
                 CurrentDriver = WebDriver2;
            }
            else if (k.Equals("WebDriver3"))
            {
                CurrentDriver = WebDriver3;
            }

        }


        [Then(@"I refresh page")]
        public void ThenIRefreshPage()
        {
            CurrentDriver.Navigate().Refresh();
            Thread.Sleep(3000);
        }




    }



}
