﻿using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TechTalk.SpecFlow;

namespace Stable_2013.Scenarios
{
    [Binding]
    class CreatingOfNewStarterProcessSteps : BaseSteps
    {

        #region DATASET

        public string candidateName = "Linda Mason"; //ENTER CANDIDATE'S NAME
        public string candidateEmail = "TestEmpl@mailinator.com";
        public string candidateSource = "Other";
        public string gender = "Female";
        public string vacancy = "QA automation";
        public string status = "In Process";
        public string jobRole = "Quality Engineer";
        public string orgUnit = "Quality Control";
        public string employeeStatus = "Active";
        public string changeReason = "New Hire";
        public DateTime assignedFrom = DateTime.Now;
        
        #endregion

        /*---------------------CREATION OF NEW CANDIDATE---------------------*/

        [Then(@"I go to RECRUITING TAB")]
        public void ThenIGoToRECRUITINGTAB()
        {
            WebDriver.GetByPath("//span/span[contains(text(), 'Recruiting')]").Click();
            WebDriver.WaitFullLoad();
        }


        [Then(@"I go to CANDIDATE DATABASE")]
        public void ThenIGoToCANDIDATEDATABASE()
        {
            Thread.Sleep(500);
            WebDriver.GetByPath("//a/h3[contains(text(), 'Candidate Database')]").Click();
        }

        [Then(@"I go to View By Vacancy")]
        public void ThenIGoToViewByVacancy()
        {
            Thread.Sleep(500);
            WebDriver.GetByPath("//a/span[contains(text(), 'View By Vacancy')]").Click();
            Thread.Sleep(1000);
        }

        [Then(@"I click on NEW ITEM")]
        public void ThenIClickOnNEWITEM()
        {
            Thread.Sleep(500);
            WebDriver.GetByPath("//tr/td/a/span[contains(text(), 'new item')]").Click();

        }

        [Then(@"I specify candidates Full Name")]
        public void ThenISpecifyCandidatesFullName()
        {
            Thread.Sleep(1500);
            WebDriver.SwitchTo().Frame(WebDriver.GetByPath("//div[2]/iframe"));
            WebDriver.GetByPath("//tbody/tr[4]/td/table/tbody/tr[1]/td[2]/span/input").SendKeys(candidateName);
            Thread.Sleep(1000);
        }


        [Then(@"I specify candidates Email")]
        public void ThenISpecifyCandidatesEmail()
        {
            WebDriver.GetByPath("//tr[6]/td/table/tbody/tr[1]/td[2]/span/input").SendKeys(candidateEmail);
            Thread.Sleep(1000);
        }


        [Then(@"I pick Candidate Source")]
        public void ThenIPickCandidateSource()
        {
            var select = new SelectElement(WebDriver.GetByPath("//tbody/tr[6]/td/table/tbody/tr[5]/td[2]/span/select"));
            select.SelectByText("" + candidateSource + "");
            Thread.Sleep(1000);
        }

        [Then(@"I pick Gender")]
        public void ThenIPickGender()
        {
            var select = new SelectElement(WebDriver.GetById("ctl00_PlaceHolderMain_g_0652780c_2ec4_40f7_865b_c875d52ae58b_ctl00_rptrMain_ctl03_ESListForm1_rptrMain_ctl01_ctlGender_DropDownChoice"));
            select.SelectByText("" + gender + "");
            Thread.Sleep(1000);
        }

        [Then(@"I click Save to save new Candidate")]
        public void ThenIClickSaveToSaveNewCandidate()
        {
            Thread.Sleep(500);
            WebDriver.GetById("ctl00_PlaceHolderMain_g_0652780c_2ec4_40f7_865b_c875d52ae58b_ctl00_btnSave").Click();
            Thread.Sleep(3000);
        }

        /*---------------------CREATION OF APPLICANT FROM CANDIDATE---------------------*/
        [Then(@"I open new Candidate Card")]
        public void ThenIOpenNewCandidateCard()
        {
            Thread.Sleep(500);
            WebDriver.GetByPath("//div/a[contains(text(), '" + candidateName + "')]").Click();
            Thread.Sleep(3000);
        }

        [Then(@"I click on Link To Vacancy")]
        public void ThenIClickOnLinkToVacancy()
        {
            Thread.Sleep(500);
            WebDriver.SwitchTo().Frame(WebDriver.GetByPath("//div[2]/iframe"));
            Thread.Sleep(1500);
            WebDriver.GetByPath("//span/a/span[2][contains(text(), 'Link to')]").Click();
            Thread.Sleep(1500);
            WebDriver.SwitchTo().DefaultContent();

        }

        [Then(@"I picking a vacancy from dropdownlist")]
        public void ThenIPickingAVacancyFromDropdownlist()
        {
            WebDriver.WaitFullLoad();
            WebDriver.SwitchTo().Frame(WebDriver.GetByPath("//div[3]/div/div[2]/iframe"));
            Thread.Sleep(1500);

            var select = new SelectElement(WebDriver.GetByPath("//div[1]/table/tbody/tr[1]/td[2]/select"));
            select.SelectByText("" + vacancy + "");
            Thread.Sleep(1000);
        }

        [Then(@"I set status to In Process")]
        public void ThenISetStatusToInProcess()
        {
            var select = new SelectElement(WebDriver.GetById("ctl00_PlaceHolderMain_g_e565898e_32aa_40d4_aea4_864899c8d335_ctl00_ddlStatuses"));
            select.SelectByText("" + status + "");
            Thread.Sleep(1000);
        }

        [Then(@"I click on Apply to vacancy button")]
        public void ThenIClickOnApplyToVacancyButton()
        {
            WebDriver.GetById("ctl00_PlaceHolderMain_g_e565898e_32aa_40d4_aea4_864899c8d335_ctl00_btnConnect").Click();
            WebDriver.SwitchTo().DefaultContent();
            Thread.Sleep(500);

        }

        [Then(@"I close Candidates Card")]
        public void ThenICloseCandidatesCard()
        {
            WebDriver.GetByPath("//div[1]/div/div[1]/span/a/span/span").Click();
            Thread.Sleep(500);

        }


        /*---------------------CREATION OF STARTER FROM APPLICANT---------------------*/


        [Then(@"I go to APPLICANT TRACKING")]
        public void ThenIGoToAPPLICANTTRACKING()
        {
            Thread.Sleep(200);
            WebDriver.GetByPath("//a/h3[contains(text(), 'Applicant Tracking')]").Click();
            Thread.Sleep(1000);
        }

        [Then(@"I go to In Process tab")]
        public void ThenIGoToInProcessTab()
        {
            WebDriver.GetByPath("//tbody/tr/td[1]/ul/li/a[contains(text(), '" + status + "')]").Click();
            Thread.Sleep(1000);                       
        }

        [Then(@"I mark with a tick a checkbox opposite to candidate")]
        public void ThenIMarkWithATickACheckboxOppositeToCandidate()
        {
            WebDriver.GetByPath("//table/tbody/tr/td/a[contains(text(), '" + candidateName + "')]/../../td[1]//input").Click();
            Thread.Sleep(1000);
            
        }

        [Then(@"I click on Batch Action button")]
        public void ThenIClickOnBatchActionButton()
        {
            WebDriver.GetByPath("//tr/td[1]/table/tbody/tr/td[2]/div/a").Click();
            Thread.Sleep(1000);
        }

        [Then(@"I select Create Starter list item")]
        public void ThenISelectCreateStarterListItem()
        {
            WebDriver.GetByPath("//tr/td[1]/table/tbody/tr/td[2]/div/ul/li/a[contains(text(), 'Create Starter')]").Click();
            Thread.Sleep(1000);
        }

        [Then(@"I set Applicant Status to Starter")]
        public void ThenISetApplicantStatusToStarter()
        {
            Thread.Sleep(3000);
            var select = new SelectElement(WebDriver.GetById("ctl00_PlaceHolderMain_g_979df3c4_ca8c_46ae_a258_ce30591588a0_ctl00_ddlStatuses"));
            select.SelectByText("Starter");
            Thread.Sleep(1000);
        }

        [Then(@"I Generate Applicant Letters InterviewInvitation")]
        public void ThenIGenerateApplicantLettersInterviewInvitation()
        {
            var select = new SelectElement(WebDriver.GetById("ctl00_PlaceHolderMain_g_979df3c4_ca8c_46ae_a258_ce30591588a0_ctl00_ddlLetterTemplates"));
            select.SelectByText("InterviewInvitation");
            Thread.Sleep(1000);
        }

        [Then(@"I click on Create Starter button")]
        public void ThenIClickOnCreateStarterButton()
        {
            WebDriver.GetById("ctl00_PlaceHolderMain_g_979df3c4_ca8c_46ae_a258_ce30591588a0_ctl00_btnChangeStatus").Click();
        }

        [Then(@"I go to APPLICANT LETTERS")]
        public void ThenIGoToAPPLICANTLETTERS()
        {
            Thread.Sleep(500);
            WebDriver.GetByPath("//a/h3[contains(text(), 'Applicant Letters')]").Click();
        }

        [Then(@"I mark with a tick a checkbox to send email")]
        public void ThenIMarkWithATickACheckboxToSendEmail()
        {
            WebDriver.GetByPath("//table/tbody/tr/td/a[contains(text(), '" + candidateName + "')]/../../td[1]//input").Click();
            Thread.Sleep(1000);
        }

        [Then(@"I click on Send Letters button")]
        public void ThenIClickOnSendLettersButton()
        {
            WebDriver.GetById("ctl00_PlaceHolderMain_g_0dbcca94_36e9_45bf_ad5b_831c866cf17f_ctl00_btnSendLetters").Click();
            Thread.Sleep(3000);
        }


        [Then(@"I go to CORE HR tab")]
        public void ThenIGoToCOREHRTab()
        {
            WebDriver.GetByPath("//span/span[contains(text(), 'Core HR')]").Click();
            WebDriver.WaitFullLoad();
        }

        [Then(@"I go to Processes tile")]
        public void ThenIGoToProcessesTile()
        {
            Thread.Sleep(500);
            WebDriver.GetByPath("//a/h3[contains(text(), 'Processes')]").Click();
        }

        [Then(@"I go to OnBoarding page")]
        public void ThenIGoToOnBoardingPage()
        {
            WebDriver.GetByPath("//div/div/ul/li/a/h3/span[contains(text(), 'OnBoarding')]").Click();
            Thread.Sleep(500);
        }


        [Then(@"I click on current Starter to Create a new Employee")]
        public void ThenIClickOnCurrentStarterToCreateANewEmployee()
        {
            WebDriver.GetByPath("//table/tbody/tr/td/a[contains(text(), '" + candidateName + "')]").Click();
            
        }

        [Then(@"I click on Create Employee button")]
        public void ThenIClickOnCreateEmployeeButton()
        {
            Thread.Sleep(500);
            WebDriver.SwitchTo().Frame(WebDriver.GetByPath("//div[2]/iframe"));
            Thread.Sleep(500);
            WebDriver.GetByPath("//span/a/span[2][contains(text(), 'Create')]").Click();
            Thread.Sleep(500);
            WebDriver.SwitchTo().DefaultContent();
        }

        [Then(@"I enter Job Role")]
        public void ThenIEnterJobRole()
        {
            WebDriver.WaitFullLoad();
            WebDriver.SwitchTo().Frame(WebDriver.GetByPath("//body/div[3]/div/div[2]/iframe"));
            Thread.Sleep(500);
            var select = new SelectElement(WebDriver.GetById("ctl00_PlaceHolderMain_g_236109b9_d8f6_43a8_b099_a0de0a23768c_ctl00_rptrMain_ctl01_ESListForm1_rptrMain_ctl03_ctlJobRole_Lookup"));
            select.SelectByText("" + jobRole + "");
            Thread.Sleep(2000);
        }

   




        [Then(@"I click OK")]
        public void ThenIClickOK()
        {
            WebDriver.GetById("ctl00_PlaceHolderMain_g_4aced8c7_6605_43e0_9f44_1bde84c5b332_ctl00_btnOk").Click();
            Thread.Sleep(500);
        }

        [Then(@"I close Starters Card")]
        public void ThenICloseStartersCard()
        {
            WebDriver.SwitchTo().DefaultContent();
            Thread.Sleep(1000);
            WebDriver.ExecuteJavaScript(" window.scrollTo(0, 0)");
            WebDriver.GetByPath("//div/div/div/span/a[2]/span").Click();
            Thread.Sleep(1000);
        }










     









    }
}
