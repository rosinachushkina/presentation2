﻿using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TechTalk.SpecFlow;
namespace Stable_2013.Scenarios
{
    [Binding]
    public class LearningCourseSteps : BaseSteps
    {
        public string learningCourseTitle = "";

        [When(@"I click on Learning tab")]
        public void WhenIClickOnLearningTab()
        {
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.FirstOrDefault());
            Thread.Sleep(500);
            WebDriver.GetByPath("//span/span[contains(text(), 'Learning')]").Click();
            WebDriver.GetByPath("//span/span[contains(text(), 'Learning')]").Click();
            Thread.Sleep(1500);
        }

        [When(@"choose Learning Catalog section")]
        public void WhenChooseLearningCatalogSection()
        {

            Thread.Sleep(500);
            WebDriver.GetByPath("//div[@class='menuItem']//span[contains(text(), 'Learning Catalog')]").Click();
            Thread.Sleep(500);
        }

        [When(@"WD3 I choose Learning Catalog section")]
        public void WhenWD3IChooseLearningCatalogSection()
        {
            Thread.Sleep(500);
            WebDriver3.GetByPath("//div[@class='menuItem']//span[contains(text(), 'Learning Catalog')]").Click();
            Thread.Sleep(500);
        }


        [When(@"click Add New learning Course")]
        public void WhenClickAddNewLearningCourse()
        {
            WebDriver.GetByPath("//div[1]/div/a[contains(text(), 'Add Course')]").Click();
        }

        [When(@"fill all requirement fields for new Learning Course")]
        public void WhenFillAllRequirementFieldsForNewLearningCourse()
        {
            var r = new HandyFunctions();
            learningCourseTitle = r.RandomStringLatin(8);

            WebDriver.SwitchTo().Frame(WebDriver.GetByPath("//div/div[2]/iframe"));
            WebDriver.GetByName("ctl00$PlaceHolderMain$g_8b564ac9_522b_4491_91d9_582c8fe04b80$ctl00$tbTitle").SendKeys(learningCourseTitle);
        }

        [When(@"save my new created Learning course")]
        public void WhenSaveMyNewCreatedLearningCourse()
        {
            WebDriver.GetByPath("//*[@value = 'Save']").Click();
        }


        [Then(@"I check that new Learning course was created")]
        public void ThenICheckThatNewLearningCourseWasCreated()
        {
            Assert.True(WebDriver.GetByPath("//a[contains(text(), '" + learningCourseTitle + "')]").Displayed);
        }

        [When(@"WD3 I click on role icon")]
        public void WhenWD3IClickOnRoleIcon()
        {
            WebDriver3.GetByPath("//a[@id='ui-id-1']/i").Click();
        }

        [When(@"WD3 I choose Employee role")]
        public void WhenWD3IChooseEmployeeRole()
        {
            WebDriver3.GetByPath("//div[@id='tabRoles']//a[contains(text(),'Employee')]").Click();
        }

        [When(@"WD3 I click on My Learning tab")]
        public void WhenWD3IClickOnMyLearningTab()
        {
            WebDriver3.SwitchTo().Window(WebDriver3.WindowHandles.FirstOrDefault());
            Thread.Sleep(500);
            WebDriver3.GetByPath("//span/span[contains(text(), 'Learning')]").Click();
            WebDriver3.GetByPath("//span/span[contains(text(), 'Learning')]").Click();
            Thread.Sleep(1500);
        }

        [When(@"WD3 I assign to new created Learning Course")]
        public void WhenWD3IAssignToNewCreatedLearningCourse()
        {
            WebDriver3.GetByPath("//a[contains(text(), '" + learningCourseTitle + "')]/..//a[contains(text(), 'Add to My Plan')]").Click();
        }

        //a[contains(text(), 'ombd')]/..//span[contains(text(), 'You have this course in your plan')]

    }
}
