﻿using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TechTalk.SpecFlow;


namespace Stable_2013.Scenarios
{
    [Binding]
    public sealed class CreationNewEmployeeSteps : BaseSteps
    {

        #region DATASET
        public string testEmployee = "Test Employee";
        public string orgUnit = "Sales";
        public string jobRole = "Recruiter";
        public string dateAssignment = DateTime.Today.AddDays(-1).ToShortDateString();
        string employeeStatus = "Active";
        string changeReason = "New Hire";
        public string terminationFrame = "//div[4]/div/div[2]/iframe";
        public string employeeFrame = "//div/iframe";

        #endregion


        [Given(@"I open main page")]
        public void GivenIOpenMainPage()
        {
            CurrentDriver.Url = Pages.MainPage;

            try
            {
                IAlert alert = CurrentDriver.SwitchTo().Alert();
                HandyFunctions.MakeLoginAs("HR");
                Thread.Sleep(3000);
            }
            catch (NoAlertPresentException e)
            { }
            CurrentDriver.WaitFullLoad();
        }


        [Given(@"I click on my name and Sign In as (.*)")]
        public void ThenIClickOnMyNameAndSignInAs(string role)
        {

            CurrentDriver.GetByPath("//span[@id='zz6_Menu_t']").Click();
            CurrentDriver.GetByPath("//span[1][contains (text(), 'Sign in as Different User')]").Click();
            HandyFunctions.MakeLoginAs(role);
            Thread.Sleep(2000);
        }

        [Given(@"I go to CORE HR tab")]
        public void GivenIGoToCOREHRTab()
        {
            CurrentDriver.GetByPath("//span/span[contains(text(), 'Core HR')]").Click();
            CurrentDriver.WaitFullLoad();
        }

        [Given(@"I press create new Employee")]
        public void GivenIPressCreateNewEmployee()
        {
            Thread.Sleep(1000);
            CurrentDriver.GetByPath("//ul[@class='subMenuActions']/li/a[contains(text(), 'Create Employee')]").Click();
        }


        [Given(@"I enter Full Name")]
        public void GivenIEnterFullName()
        {
            CurrentDriver.SwitchTo().Frame(CurrentDriver.GetByPath("//div/div[2]/iframe"));
            CurrentDriver.GetByPath("//table//tr[6]//tr[1]//span/input").SendKeys(testEmployee);
            //no hardcode here - use variable to enter employee FullName
        }

        [Given(@"I enter Job Role")]
        public void GivenIEnterJobRole()
        {
            var select = new SelectElement(CurrentDriver.GetById("ctl00_PlaceHolderMain_g_236109b9_d8f6_43a8_b099_a0de0a23768c_ctl00_rptrMain_ctl01_ESListForm1_rptrMain_ctl03_ctlJobRole_Lookup"));
            select.SelectByText("" + jobRole + "");
            Thread.Sleep(3000);
            //you have to invent the method to select JobRole
        }

        [Given(@"I enter Org Unit")]
        public void GivenIEnterOrgUnit()
        {
            IWebElement orgUnitField = CurrentDriver.GetByPath("//tbody/tr[9]/td/table/tbody/tr[3]/td[2]//td[1]/table/tbody/tr/td/div");
            orgUnitField.SendKeys(orgUnit);
            orgUnitField.SendKeys(OpenQA.Selenium.Keys.Enter);
            //you have to invent the method to select OrgUnit
        }

        [Given(@"I click SAVE")]
        public void GivenIClickSAVE()
        {
            Thread.Sleep(1000);
            CurrentDriver.GetById("ctl00_PlaceHolderMain_g_236109b9_d8f6_43a8_b099_a0de0a23768c_ctl00_btnSaveTop").Click();

        }

        [Given(@"I set employee status to ACTIVE")]
        public void GivenISetEmployeeStatusToACTIVE()
        {
            var select = new SelectElement(CurrentDriver.GetByPath("//tbody/tr[5]/td[2]/select"));
            select.SelectByText(employeeStatus);

        }

        [Given(@"I picking the date ASSIGNED FROM")]
        public void GivenIPickingTheDateASSIGNEDFROM()
        {
            CurrentDriver.GetByPath("//tbody/tr[7]/td[2]/table/tbody/tr/td[1]/input").SendKeys(dateAssignment);
        }

        [Given(@"I picking CHANGE REASON to NEW HIRE")]
        public void GivenIPickingCHANGEREASONToNEWHIRE()
        {
            var select = new SelectElement(CurrentDriver.GetByPath("//tbody/tr[9]/td[2]/select"));
            select.SelectByText(changeReason);
        }

        [Given(@"I click OK")]
        public void GivenIClickOK()
        {
            CurrentDriver.GetByPath("//td/input[@value='OK']").Click();
        }

        [Then(@"I go to Employees tile")]
        public void ThenIGoToEmployeesTile()
        {
            Thread.Sleep(500);
            CurrentDriver.GetByPath("//a/h3/span[contains(text(), 'Employees')]").Click();
        }

        [Then(@"I enter a name of new Employee")]
        public void ThenIEnterANameOfNewEmployee()
        {
            CurrentDriver.GetById("ctl00_PlaceHolderMain_g_07447ff5_2f52_4d9b_8a46_35dfacb142ea_ctl00_EmployeeFilter1_keyword").SendKeys(testEmployee);
        }


        [Then(@"I click View button")]
        public void ThenIClickViewButton()
        {
            CurrentDriver.GetById("ctl00_PlaceHolderMain_g_07447ff5_2f52_4d9b_8a46_35dfacb142ea_ctl00_EmployeeFilter1_btnSubmit").Click();
            Thread.Sleep(1000);
        }

        [Then(@"I click on name of created Employee")]
        public void ThenIClickOnNameOfCreatedEmployee()
        {
            CurrentDriver.GetByPath("//div[1]/table//a/span[contains(text(), '" + testEmployee + "')]").Click();
        }

        [Then(@"I switch to Employee frame")]
        public void ThenISwitchToEmployeeFrame()
        {
            CurrentDriver.SwitchTo().Frame(CurrentDriver.GetByPath(employeeFrame));
        }

        [Then(@"I click Terminate Employee")]
        public void ThenIClickTerminateEmployee()
        {
            CurrentDriver.GetByPath("//a/span[contains(text(), 'Terminate')]").Click();
            CurrentDriver.SwitchTo().DefaultContent();
        }

        [Then(@"I fill all requirement fields for termination")]
        public void ThenIFillAllRequirementFieldsForTermination()
        {
            
            CurrentDriver.SwitchTo().Frame(CurrentDriver.GetByPath(terminationFrame));
            CurrentDriver.GetByPath("//tr[2]//tbody/tr/td[1]/input").SendKeys(dateAssignment);
            CurrentDriver.GetByPath("//*[@value='Terminate']").Click();
        }
    }
}

