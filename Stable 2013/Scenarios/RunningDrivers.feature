﻿Feature: RunningDrivers
	In order to running different drivers
	And switch between them

@runningDrivers
Scenario: RunningDrivers test
	Given I open main page
	And I click on my name and Sign In as Manager
	
	Then I want to run WebDriver2
	Then I activate WebDriver2
	Given I open main page
	And I click on my name and Sign In as Employee

	Then I activate WebDriver
	Then I refresh page
	