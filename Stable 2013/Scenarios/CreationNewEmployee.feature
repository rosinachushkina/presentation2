﻿Feature: CreationNewEmployee
	To create New Employee under HR Role

@creationNewEmployee
Scenario: CreationNewEmployee
	Given I open main page
	And I click on my name and Sign In as HR
	And I go to CORE HR tab
	And I press create new Employee
	
	And I enter Full Name
	And I enter Org Unit
	And I click SAVE
	
	And I set employee status to ACTIVE
	And I picking the date ASSIGNED FROM
	And I picking CHANGE REASON to NEW HIRE
	And I click OK

	And I go to CORE HR tab
	Then I go to Employees tile
	Then I enter a name of new Employee
	Then I click View button
	Then I click on name of created Employee
	Then I switch to Employee frame
	Then I click Terminate Employee
	Then I fill all requirement fields for termination



	
	


