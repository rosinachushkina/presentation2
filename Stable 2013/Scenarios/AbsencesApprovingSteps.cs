﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;
using System.Windows.Forms;
using System.Diagnostics;
using OpenQA.Selenium.IE;

namespace Stable_2013.Scenarios
{
    [Binding]
    public sealed class AbsencesApprovingSteps : BaseSteps
    {
        string site;
        private string todaysDate = DateTime.Today.ToShortDateString();
        private string tommorowDate = DateTime.Today.AddDays(1).ToShortDateString();
        private string duration = "";
        







    







    
        [Then(@"I click on Role icon")]
        public void ThenIClickOnRoleIcon()
        {
            CurrentDriver.GetByPath("//a[@id='ui-id-1']/i").Click();
        }

       


        [Then(@"I choose Employee role")]
        public void ThenIChooseEmployeeRole()
        {
            CurrentDriver.GetByPath("//div[@id='tabRoles']//a[contains(text(),'Employee')]").Click();
        }

        [Then(@"I click on Attendance block")]
        public void ThenIClickOnAttendanceBlock()
        {
            Thread.Sleep(500);
            CurrentDriver.GetByPath("//div[@id='topMenuContainer']//span/span[contains(text(), 'Attendance')]").Click();
            Thread.Sleep(500);
        }

        [Then(@"I click on My Absences section")]
        public void ThenIClickOnMyAbsencesSection()
        {
            Thread.Sleep(500);
            CurrentDriver.GetByPath("//div[@class='menuItem']//h3[contains(text(), 'My Absences')]/../img").Click();
        }
        [Then(@"I click Add Vacation Request")]
        public void ThenIClickAddVacationRequest()
        {
            Thread.Sleep(500);
            CurrentDriver.GetByPath("//td/a[contains(text(), 'Add Vacation Request')]").Click();
        }

        [Then(@"I choose Today's Date for Start Date")]
        public void ThenIChooseTodaySDateForStartDate()
        {
            CurrentDriver.GetByPath("//nobr[contains(text(), 'Start Date')]/../../../td[2]//input").
                Clear();
            CurrentDriver.GetByPath("//nobr[contains(text(), 'Start Date')]/../../../td[2]//input").
                SendKeys(todaysDate);
        }

        [Then(@"I choose next of Today's Date for Return to Work Date")]
        public void ThenIChooseNextOfTodaySDateForReturnToWorkDate()
        {
            CurrentDriver.GetByPath("//nobr[contains(text(), 'Return to Work Date')]/../../../td[2]//input").
                SendKeys(tommorowDate);
        }

        [Then(@"I click Calculate Duration")]
        public void ThenIClickCalculateDuration()
        {
            CurrentDriver.GetByPath("//*[@value='Calculate Duration']").Click();
            string d = CurrentDriver.GetByPath("//table//table[2]/tbody/tr[1]/td[2]/span[1]").Text;
            string h = CurrentDriver.GetByPath("//table//table[2]/tbody/tr[1]/td[2]/span[2]").Text;
            duration = "" + d + " d / " + h + " hrs";

        }

        [Then(@"I switch to absence iframe")]
        public void ThenISwitchToAbsenceIframe()
        {
            CurrentDriver.SwitchTo().Frame(CurrentDriver.GetByPath("//div[1]/div/div[2]/iframe"));
        }

        [Then(@"I click Save")]
        public void ThenIClickSaveMyAbsences()
        {
            CurrentDriver.GetByPath("//*[@value='Save']").Click();
            Thread.Sleep(2000);
        }

        [Then(@"I check that Vacation was created")]
        public void ThenICheckThatVacationWasCreated()
        {
            string durationInTab = CurrentDriver.GetByPath("//span[contains(text(), 'Vacation')]/../../td[5]/span").Text;
            string status = CurrentDriver.GetByPath("//span[contains(text(), 'Vacation')]/../../td[6]/span").Text;
            string startDate = (DateTime.Parse(CurrentDriver.GetByPath("//span[contains(text(), 'Vacation')]/../../td[3]/span").Text).ToShortDateString());
            string returnToWork = DateTime.Parse(CurrentDriver.GetByPath("//span[contains(text(), 'Vacation')]/../../td[4]/span").Text).ToShortDateString();


            Assert.True(startDate.Equals(todaysDate));//startDate is correct
            Assert.True(returnToWork.Equals(tommorowDate));//returnToWorkDate is correct
            Assert.True(duration.Equals(durationInTab));//Duration is correct
            Assert.True(status.Equals("Pending"));

        }

        [Then(@"I click on cogwheel")]
        public void ThenIClickOnCogwheel()
        {
            CurrentDriver.GetById("zz14_SiteActionsMenu").Click();
        }

        [Then(@"I open Site Content")]
        public void ThenIOpenSiteContent()
        {
            CurrentDriver.GetByPath("//span[contains(text(), 'Site contents')]").Click();
        }

        [Then(@"I choose (.*) list")]
        public void ThenIChooseVacationsList(string list)
        {
            CurrentDriver.GetByPath("//a[@title='" + list + "']").Click();
        }

        [Then(@"I select checkbox Vacation of Employee")]
        public void ThenISelectCheckboxVacationOfEmployee()
        {
            CurrentDriver.GetByPath("//a[2][contains(text(), '" + Variables.employeeName + "')]/preceding-sibling::a/span").Click();
            CurrentDriver.GetByPath("//a[contains(text(), '" + Variables.employeeName + "')]/../../td[@tabindex='0']").Click();
        }

        [Then(@"I click on ITEMS")]
        public void ThenIClickITEMS()
        {
            CurrentDriver.GetByPath("//span[contains(text(), 'Items')]").Click();
        }

        [Then(@"I click on DELETE ITEM")]
        public void ThenIClickOnDELETEITEM()
        {
            Thread.Sleep(600);
            CurrentDriver.GetByPath("//span[2][contains(text(), 'Delete Item')]").Click();
            Thread.Sleep(600);
            CurrentDriver.SwitchTo().Alert().Accept();
        }

        [Then(@"I choose Manager role")]
        public void ThenIChooseManagerRole()
        {
            CurrentDriver.GetByPath("//div[@id='tabRoles']//a[contains(text(),'Manager')]").Click();
        }

      
        [Then(@"I click on Absences section")]
        public void ThenIClickOnAbsencesSection()
        {
            CurrentDriver.GetByPath("//div[@class='menuItem']//h3[contains(text(), 'Absences')]").Click();
        }

        [Then(@"I check created by Employee Vacation")]
        public void ThenICheckCreatedByEmployeeVacation()
        {
            string foundName = CurrentDriver.GetByPath("//tbody//td[4]/span[contains(text(), 'Vacation')]/../../td[1]/a").Text;
            string startDate = (DateTime.Parse(CurrentDriver.GetByPath("//tbody//td[4]/span[contains(text(), 'Vacation')]/../../td[6]/span").Text).ToShortDateString());
            string returnToWork = DateTime.Parse(CurrentDriver.GetByPath("//tbody//td[4]/span[contains(text(), 'Vacation')]/../../td[7]/span").Text).ToShortDateString();
            string durationInTab = CurrentDriver.GetByPath("//tbody//td[4]/span[contains(text(), 'Vacation')]/../../td[8]/span").Text;


            Assert.True(startDate.Equals(todaysDate));//startDate is correct
            Assert.True(returnToWork.Equals(tommorowDate));//returnToWorkDate is correct
            Assert.True(duration.Equals(durationInTab));//Duration is correct
            Assert.True(foundName.Equals(Variables.employeeName));
        }

        [Then(@"I approve checked Vacation")]
        public void ThenIApproveCheckedVacation()
        {
            CurrentDriver.GetByPath("//tbody//td[4]/span[contains(text(), 'Vacation')]/../../td[15]/a[2]").Click();
        }

        [Then(@"I switch to approve/reject iFrame")]
        public void ThenISwitchToApproveRejectIFrame()
        {
            CurrentDriver.SwitchTo().Frame(CurrentDriver.GetByPath("//div/div[2]/iframe"));

        }

        [Then(@"I approve this Vacation")]
        public void ThenIApproveThisVacation()
        {
            var select = new SelectElement(CurrentDriver.GetByPath("//select"));
            select.SelectByText("Approve");
        }


        [Then(@"I check that Vacation is approved")]
        public void ThenICheckThatVacationIsApproved()
        {
            string status = CurrentDriver.GetByPath("//span[contains(text(), 'Vacation')]/../../td[6]/span").Text;
            Assert.True(status.Equals("Approved"));
        }


        [Then(@"I click Add Sickness Registration")]
        public void ThenIClickAddSicknessRegistration()
        {
            CurrentDriver.GetByPath("//a[@href='" + Pages.MainPage + "SitePages/Absence/AddSickness.aspx?InitID=58']").Click();
        }

        [Then(@"I switch to sickness iframe")]
        public void ThenISwitchToSicknessIframe()
        {
            CurrentDriver.SwitchTo().Frame(CurrentDriver.GetByPath("//div[2]/div/div[2]/iframe"));
        }
        

        [Then(@"I check that Sickness was created")]
        public void ThenICheckThatSicknessWasCreated()
        {
            string durationInTab = CurrentDriver.GetByPath("//span[contains(text(), 'Sickness')]/../../td[5]/span").Text;
            string status = CurrentDriver.GetByPath("//span[contains(text(), 'Sickness')]/../../td[6]/span").Text;
            string startDate = (DateTime.Parse(CurrentDriver.GetByPath("//span[contains(text(), 'Sickness')]/../../td[3]/span").Text).ToShortDateString());
            string returnToWork = DateTime.Parse(CurrentDriver.GetByPath("//span[contains(text(), 'Sickness')]/../../td[4]/span").Text).ToShortDateString();
          
            Assert.True(startDate.Equals(todaysDate));//startDate is correct
            Assert.True(returnToWork.Equals(tommorowDate));//returnToWorkDate is correct
            Assert.True(duration.Equals(durationInTab));//Duration is correct
            Assert.True(status.Equals("Pending"));
        }

        [Then(@"I check created by Employee Sickness")]
        public void ThenICheckCreatedByEmployeeSickness()
        {
            string foundName = CurrentDriver.GetByPath("//tbody//td[4]/span[contains(text(), 'Sickness')]/../../td[1]/a").Text;
            string startDate = (DateTime.Parse(CurrentDriver.GetByPath("//tbody//td[4]/span[contains(text(), 'Sickness')]/../../td[6]/span").Text).ToShortDateString());
            string returnToWork = DateTime.Parse(CurrentDriver.GetByPath("//tbody//td[4]/span[contains(text(), 'Sickness')]/../../td[7]/span").Text).ToShortDateString();
            string durationInTab = CurrentDriver.GetByPath("//tbody//td[4]/span[contains(text(), 'Sickness')]/../../td[8]/span").Text;


            Assert.True(startDate.Equals(todaysDate));//startDate is correct
            Assert.True(returnToWork.Equals(tommorowDate));//returnToWorkDate is correct
            Assert.True(duration.Equals(durationInTab));//Duration is correct
            Assert.True(foundName.Equals(Variables.employeeName));
        }
        


 


    }
}
