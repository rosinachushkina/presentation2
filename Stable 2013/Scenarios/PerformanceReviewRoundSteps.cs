﻿using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TechTalk.SpecFlow;


namespace Stable_2013.Scenarios
{

    [Binding]
    public class PerformanceReviewRoundSteps : BaseSteps
    {
        string perfTitle = "PerformanceReviewRoundToTest";

        #region GlobalVariables

        DateTime StartDate = DateTime.Now;
        DateTime EndDate = DateTime.Now.AddDays(3);

        #endregion


        [Then(@"I navigate to the Performance Tab")]
        public void ThenINavigateToThePerformanceTab()
        {
            WebDriver.GetByPath("//span/span[contains(text(), 'Performance')]").Click();
            WebDriver.WaitFullLoad();
        }

        [Then(@"I choose 'Performance Reviews' tile from the pop-up menu")]
        public void ThenIChooseTileFromThePop_UpMenu()
        {
            WebDriver.GetById("ctl00_ctl29_mnu2_rptrHorizontalMenuModern_ctl05_rptrHorizontalMenuItemsModern_ctl01_aMenuItem").Click();
            WebDriver.WaitFullLoad();
        }


        [Then(@"I click on Start Review Round button")]
        public void ThenIClickOnStartReviewRoundButton()
        {
            WebDriver.GetById("ctl00_PlaceHolderMain_g_0c80f4fe_e748_4556_a3a6_3e2a6e3c20f2_ctl00_hlAddItem").Click();
            WebDriver.WaitFullLoad();
            Thread.Sleep(2000);
        }

        [Then(@"I enter Performance Review Title")]
        public void ThenIEnterPerformanceReviewTitle()
        {
            WebDriver.SwitchTo().Frame(WebDriver.GetByPath("//div/div[2]/iframe"));
            Thread.Sleep(2000);
            WebDriver.GetById("Title_fa564e0f-0c70-4ab9-b863-0177e6ddd247_$TextField").SendKeys(perfTitle);
            Thread.Sleep(2000);
        }

        [Then(@"I enter Start Date")]
        public void ThenIEnterStartDate()
        {
            var shortDate = StartDate.ToString("MM/dd/yyyy");

            WebDriver.GetById("StartDate_18907cdc-bd0e-4f21-8632-5cc1fede60a3_$DateTimeFieldDate").SendKeys(shortDate);

            Thread.Sleep(1000);
        }

        [Then(@"I enter Due Date of Performance Review")]
        public void ThenIEnterDueDateOfPerformanceReview()
        {
            var dueDate = EndDate.ToString("MM/dd/yyyy");

            WebDriver.GetById("DueDate_0e702194-28f2-4a1e-9f8e-ecc36cfecf81_$DateTimeFieldDate").SendKeys(dueDate);

            Thread.Sleep(1000);
        }

    }
}

















