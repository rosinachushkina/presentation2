﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;
using System.Windows.Forms;
using System.Diagnostics;


namespace Beta_2013.Scenarios
{
    [Binding]
    public sealed class AlertTestSteps: BaseSteps
    {
        [Given(@"I open main page and make logout")]
        public void GivenIOpenMainPageAndMakeLogout()
        {
            WebDriver.Go("http://es2013par:2522/es/");
            Thread.Sleep(500);
            WebDriver.GetById("zz6_Menu").Click();
            WebDriver.GetByPath("//span[1][contains (text(), 'Sign in as Different User')]").Click();
            Thread.Sleep(5000);
        }

        [Then(@"I try to work with alert")]
        public void ThenITryToWorkWithAlert()
        {
            Process.Start("C:/Users/par/Desktop/test.exe");
          /*  WebDriver.SwitchTo().Alert().SendKeys("user3");
            SendKeys.SendWait( @"{TAB}" );
            SendKeys.SendWait("Pa$$w0rd");
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(10000); */
            Thread.Sleep(8000);
            
            //C:\Users\par\Desktop\script.exe
        }

        
    }
}
