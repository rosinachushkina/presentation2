﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace Beta_2013.Scenarios
{
    [Binding]
    public sealed class CheckingAbsancePlans : BaseSteps
    {
        // Переменные которые я исользую для всех методов класса  CheckingAbsancePlans
        int NumberofDaysorHours = 18;
        int RangeStart1 = 0;
        int RangeEnd2 = 2;
        int RangeStart3 = 3;
        int RangeEnd4 = 4;
        int RangeStart5 = 0;
        int RangeEnd6 = 40;
        int RangeStart7 = 41;
        int RangeEnd8 = 100;
        String CalculationType1 = "Age";
        String CalculationType2 = "Seniority";


        // Шаг выбора любого контейнера из топ меню
        [Given(@"I select '(.*)' from topMenuContainer")]
        public void GivenISelectFromMainMenu(string p0)
        {
            WebDriver.GetByPath("//div[@id='topMenuContainer']//span[contains(text(),'"+ p0 +"')]").Click();
            Thread.Sleep(1000);
        }
        // Шаг выбора любого item из menu items
        [Given(@"I select '(.*)' from menuItem")]
        public void GivenISelectFrom(string p0)
        {
            WebDriver.GetByPath("//div[@class='menuItem']//h3[contains(text(),'" + p0 + "')]").Click();
            Thread.Sleep(1000);
        }

        [Given(@"I open Plan Details")]
        public void GivenIOpenPlanDetails()
        {
            WebDriver.GetByPath("//*[@id='ctl00_PlaceHolderMain_g_d9442895_10f3_4638_82b4_19023625455f_ctl00_gvPlans_ctl02_lnkDetails']").Click();
            Thread.Sleep(1000);
        }
        // Задаю настройки для Absence Plan Sections - Test Absence Plan - Vacation
        [Given(@"I open Section Settings")]
        public void GivenIOpenSectionSettings()
        {
            WebDriver.GetByPath("//*[@id='ctl00_PlaceHolderMain_g_697bddce_c3c2_47f1_8092_62ea2df33636_ctl00_tdVacationEditSettings']/a").Click();
            Thread.Sleep(1000);
        }
        // Шаг входа в iframe 
        [Given(@"I go to iframe '(.*)'")]
        public void GivenIGoToIframe(string p0)
        {
            WebDriver.SwitchTo().Frame(WebDriver.GetByPath(""+p0+""));
        }
        // Шаг выхода из iframe
        [Given(@"I logout from iframe")]
        public void GivenILogoutFromIframe()
        {
            WebDriver.SwitchTo().DefaultContent();
            Thread.Sleep(1000);
        }



        [Given(@"i set '(.*)' Accrual Period")]
        public void GivenISetAccrualPeriod(String p0)
        {
            var option = new SelectElement(WebDriver.GetByPath("//*[@id='AccrualMethod_46211e3b-3670-48ab-a09f-8d22b79ff5f9_$DropDownChoice']"));
            option.SelectByText(p0);
            Thread.Sleep(100);
        }

        [Given(@"I save my choice")]
        public void GivenISaveAbsencePlanSections()
        {
            WebDriver.GetByPath("//*[@id='ctl00_ctl27_g_3deb8696_b0b0_4d0c_9347_6fb47bc7ea75_ctl00_toolBarTbl_RightRptControls_ctl00_ctl00_diidIOSaveItem']").Click();
            Thread.Sleep(1000);
        }


        // Добавление правил для Seniority
        [Given(@"I open Allowance Rule")]
        public void GivenIOpenAllowanceRule()
        {
            WebDriver.GetByPath("//*[@id='ctl00_PlaceHolderMain_g_697bddce_c3c2_47f1_8092_62ea2df33636_ctl00_tdAddVacationAllowance']/a").Click();
            Thread.Sleep(1000);

        }

        [Given(@"I fill Number of Days or Hours '(.*)'")]
        public void GivenIFillNumberOfDaysOrHours(int p0)
        {
            WebDriver.GetByPath("//*[@id='NumberDays_3c640b69-5502-4bbd-a0b4-806e4b84f94b_$NumberField']").Click();
            WebDriver.GetByPath("//*[@id='NumberDays_3c640b69-5502-4bbd-a0b4-806e4b84f94b_$NumberField']").SendKeys(p0.ToString());
            
            
        }
        [Given(@"I fill Range Start '(.*)'")]
        public void GivenIFillRangeStart(int p0)
        {
            WebDriver.GetByPath("//*[@id='RangeStart_47711f44-b721-4398-9688-8a14fd8fcea2_$NumberField']").Click();
            WebDriver.GetByPath("//*[@id='RangeStart_47711f44-b721-4398-9688-8a14fd8fcea2_$NumberField']").SendKeys(p0.ToString());
        }
        [Given(@"i fill Range End '(.*)'")]
        public void GivenIFillRangeEnd(int p0)
        {
            WebDriver.GetByPath("//*[@id='RangeEnd_fbc51b31-0651-40a0-be91-fcd945fb2eb4_$NumberField']").Click();
            WebDriver.GetByPath("//*[@id='RangeEnd_fbc51b31-0651-40a0-be91-fcd945fb2eb4_$NumberField']").SendKeys(p0.ToString());
        }

     

        [Given(@"i fill Calculation Type '(.*)'")]
        public void GivenIFillCalculationType(string p0)
        {
            var option = new SelectElement(WebDriver.GetByPath("//*[@id='CalculationType_762ba141-b18d-4b3f-9f42-1cfb84a1a299_$DropDownChoice']"));
            option.SelectByText(p0);
            Thread.Sleep(1000);
            
        }
        [Given(@"I save my rule choice")]
        public void GivenISaveMyRuleChoice()
        {
            WebDriver.GetByPath("//*[@id='ctl00_ctl27_g_1469c379_7104_4d73_a270_e2ae337ca00a_ctl00_toolBarTbl_RightRptControls_ctl00_ctl00_diidIOSaveItem']").Click();
            Thread.Sleep(10000);
        }
       


     
    }
}
