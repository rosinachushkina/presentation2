﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Common;

namespace HaulMatch.Scenarios
{
    [Binding]
    public sealed class Stable : BaseSteps
    {
        [Given(@"I open main page '(.*)'")]
        public void GivenIOpenMainPage(string p0)
        {
            WebDriver.Url = p0;
        }

        [Given(@"i login to system as HR")]
        public void GivenILoginToSystemAsHR()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"i verify HR parameters")]
        public void ThenIVerifyHRParameters()
        {
            ScenarioContext.Current.Pending();
        }



    }
}
